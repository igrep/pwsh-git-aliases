# 0.1.1

- Fix: `git --help -av` seems the right command to collect git subcommands.  
  This change makes `Get-GitCommands` works with Git later than v2.20.
- Fix: Specify root module properly. This might fix the problem that  
  `Install-Module -Name GitAliases` doesn't install actually.

# 0.1.0

- Initial release.
