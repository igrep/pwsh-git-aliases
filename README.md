# GitAliases

Make alias functions for the all git subcommands to omit typing `git`.

## Supported Git Version

v2.20 or later.  
(Only versions I'm currently using. Fork and fix by yourself if it doesn't work with your version of Git.)

## Intallation

### Dependencies

- [posh-git](https://www.powershellgallery.com/packages/posh-git/)

```ps1
Install-Module -Name GitAliases
```

## Usage

Add this snippet to your `$profile`:

```ps1
Import-Module GitAliases
```

## Known Issue

`Enable-GitAliases` (without `-AllowClobber` option) is slow. So you might prefer defining a function to call `Enable-GitAliases` only when necessary (like me!):

```ps1
function gim() {
  Enable-GitAliases -AllowClobber

  # Existing aliases can't be overridden even with -AllowClobber.
  # So you have to remove them.
  Remove-Item -Force alias:gc
  Remove-Item -Force alias:rm
  Remove-Item -Force alias:diff
  Remove-Item -Force alias:mv
}
```
